// Brian Cabacungan
// March 23, 2021
// JScript 310B - Final Project
// UW - Winter 2021


//Class feature
//Creates a zipCode class that the user can create of KNOWN ZipCode sales tax percentages
class zipCode {
    constructor(location, taxPrice) {
        this.location = location;
        this.taxPrice = taxPrice;
    }
};

//Creates 2 ZipCode objects
let SD = new zipCode('92115', 7.75);
let SEA = new zipCode('98101', 10.00);


//Calculate SubTotal function
//Returns SubTotal for further calculation
//Uses timing functions to alert user of form validations
//Calculated SubTotal is returned in the proper form field
function calcSub() {
    let price = document.getElementById("price").value;
    let quantity = document.getElementById("quantity").value;

    //if statements check for form validations
    if (price == "" || !price.match(/^\d+(?:\.\d{0,2})$/)) {
        setTimeout(timingAlert1, 3000); //calls timing function to show proper alert
    }

    if (quantity == "" || !quantity.match(/^\d+$/)) {
        setTimeout(timingAlert2, 3000); //calls timing function to show proper alert
    }

    else {
        subTotal = price * quantity;
        document.getElementById("subtotal").value = subTotal.toFixed(2); //2 decimal places
    
        return subTotal; //output SubTotal
    }

};


//Timing function 1
function timingAlert1() {
    alert("Please enter a proper dollar amount for 'Price'. (must enter a decimal value)");
};


//Timing function 2
function timingAlert2() {
    alert("Plese enter a whole number for 'Quantity'.");
};


//Calculate GrandTotal function
//Returns GrandTotal of item(s)
//Uses timing function to alert user of form validations
//Calculated GrandTotal is returned in the proper form field
function calcGrand() {
    let subTot = calcSub(); //calls calcSub function to calculate SubTotal
    let taxPercent = (document.getElementById("tax").value); 
    let taxAmt = taxPercent / 100;

    //if statements check for form validations
    if (taxPercent == "" || !taxPercent.match(/^\d+(?:\.\d{0,2})$/)) {
        setTimeout(timingAlert3, 3000); //calls timing function to show proper alert
    }

    else {
        taxTot = subTot * taxAmt;
        grandTotal = subTot + taxTot;

        document.getElementById("total").value = grandTotal.toFixed(2); //2 decimal places

        return grandTotal; //output GrandTotal
    }
};

//Timing function 3
function timingAlert3() {
    alert("Please enter percent numbers for 'Tax'. (ex. 7.75)");
};


//Auto outputs proper Sales Tax percentage on form field according to entered ZipCode
//Refers to KNOWN ZipCodes; 2 ZipCode objects are made in the program for the KNOWN ZipCodes
//Checks to see if a ZipCode is entered or if it matches any of the ZipCodes of the 2 ZipCode objects
//Proceeds to calculate GrandTotal properly as if no known ZipCode is entered by the user
function zipLocation() {
    let subTot = calcSub(); //calls calcSub function to calculat SubTotal
    let taxPercent = (document.getElementById("tax").value);

    //if statements check to see if ZipCode matches any of the 2 ZipCode objects
    if (document.getElementById("zipcode").value == SD.location) {
        document.getElementById("tax").value = '7.75';
        
        taxTot = subTot * (SD.taxPrice / 100);
        grandTotal = subTot + taxTot;

        document.getElementById("total").value = grandTotal.toFixed(2); //2 decimal places

        return grandTotal; //output GrandTotal
    }

    if (document.getElementById("zipcode").value == SEA.location) {
        document.getElementById("tax").value = '10.00';
        
        taxTot = subTot * (SEA.taxPrice / 100);
        grandTotal = subTot + taxTot;

        document.getElementById("total").value = grandTotal.toFixed(2); //2 decimal places

        return grandTotal; //output GrandTotal
    }

    if (document.getElementById("zipcode").value == "" && document.getElementById("tax").value != "") {
        calcGrand();
    }

    else {
        setTimeout(timingAlert4, 3000); //calls timing function to show proper alert
        calcGrand(); //calls calcGrand function to calculate GrandTotal
    }

};

//Timing function 4
function timingAlert4() {
    alert("Unknown ZipCode. Please enter tax percent value. (ex. 7.75)");
};